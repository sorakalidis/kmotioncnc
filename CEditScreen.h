/*
	CEditScreen

	CEdit extended for custom Fonts

	Copyright:Dynomotion, Inc. 2016
*/

#pragma once


class CEditScreen;
typedef CEditScreen *LPCEditScreen;


class CEditScreen : public CEdit
{
	//	DECLARE_DYNAMIC(CEditScreen)

public:

	CEditScreen();
	virtual ~CEditScreen();
	void SetFont(const char *szFaceName, int height, bool Bold, bool Italic);
	CStringW ToolTipText;
	int Var;

	static CList <LPCEditScreen, LPCEditScreen> EditScreens;




protected:
	CFont m_font;
	// Overrides
	 // ClassWizard generated virtual function overrides
	 //{{AFX_VIRTUAL(CEditScreen)
public:
	//}}AFX_VIRTUAL
  // Generated message map functions
protected:
	//{{AFX_MSG(CEditScreen)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


