// CEditScreen.cpp : implementation file
//
/*
	Unicode Button
	�2005 Robbert E. Peters

	History:
				Version 1.0 Date: 25/09/2005

	Usage:
		Create an owner-draw button control
		Add a member for it and change CButton 2 CCEditScreen
		Set the Text/Styles

		Link with Usp10.lib

	Copyright:
				You may use this code anyway you like.
*/

#include "stdafx.h"
#include "CEditScreen.h"

#include "Usp10.h"


CList <LPCEditScreen, LPCEditScreen> CEditScreen::EditScreens;


// CEditScreen

//IMPLEMENT_DYNAMIC(CEditScreen, CButton)
CEditScreen::CEditScreen()
{
	ToolTipText = "";
	Var = -1;
	CEditScreen::EditScreens.AddTail(this);
}



CEditScreen::~CEditScreen()
{
	m_font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CEditScreen, CButton)
	//{{AFX_MSG_MAP(CColorButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



void CEditScreen::SetFont(const char *szFaceName, int height, bool Bold, bool Italic)
{
	m_font.DeleteObject();
	m_font.CreateFont(height, 0, 0, 0, Bold ? FW_BOLD : FW_NORMAL, Italic, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, szFaceName);
	CEdit::SetFont(&m_font);
}


